using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    [SerializeField]
    Connector connector;
    [SerializeField]
    Transform spawnPoint;
    
    void Start()
    {
        Invoke("SpawnPlayer", 0.15f);
    }

    void SpawnPlayer()
    {
        connector.playerController.SpawnPlayer(spawnPoint);
    }
}
