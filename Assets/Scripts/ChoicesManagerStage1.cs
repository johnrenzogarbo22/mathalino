using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChoicesManagerStage1 : MonoBehaviour
{
    public QuestionManager questionManager;
    public GameObject[] choiceHolder;

    public void DisplayChoices()
    {
        var choices = questionManager.currentQuestion.choices;
        for(var i=0; i<choices.Count; i++)
        {
            choiceHolder[i].SetActive(true);
            choiceHolder[i].GetComponent<ChoiceHolder>().SetText(choices[i]);
        }
    }
}
