using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class QuestionManager : MonoBehaviour
{
    [SerializeField]
    Connector connector;
    Question[] questions;
    [SerializeField]
    int stage = 1;
    [SerializeField]
    int[] levels = { 1 };



    [SerializeField]
    GameObject questionTrigger;

    public Question currentQuestion = new();

    [SerializeField]
    UnityEvent onShowQuestion = new UnityEvent();
    private void Start()
    {
        Invoke("InitializeQuestions", 0.12f);
    }
    public void InitializeQuestions()
    {      
        questions = connector.questionData.GenerateQuestions(stage, levels);      
    }

    public void GetQuestion()
    {
        if (questions.Length > 0)
        {
            currentQuestion = questions[Random.Range(0, questions.Length)];          
            Debug.Log("Random question: " + currentQuestion.question); 
        }
        else
        {
            Debug.Log("No questions available.");
        }
        questionTrigger.SetActive(false);
        connector.uiController.DisplayQuestion(currentQuestion.question);
        onShowQuestion?.Invoke();
    }
}
