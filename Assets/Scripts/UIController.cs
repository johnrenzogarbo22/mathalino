using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class UIController : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI questionTxt;

    public void DisplayQuestion(string x)
    {
        questionTxt.gameObject.SetActive(true);
        questionTxt.text = x;
    }

    public void RemoveQuestion()
    {
        questionTxt.gameObject.SetActive(false);
        questionTxt.text = "";
    }
}
