using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public GameObject player;
    public GameObject gameData;
    MovementController movement;
   
    void Start()
    {
        gameData = GameObject.Find("GameData");
    }

    public void SpawnPlayer(Transform x)
    {
        player = Instantiate(player, x.position, x.rotation);
        movement = player.GetComponent<MovementController>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Walk(int x)
    {
        movement.Walk(x);
    }

    public void Jump()
    {
        movement.Jump();
    }
}
