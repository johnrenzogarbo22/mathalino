using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

public class QuestionsData : MonoBehaviour
{
    [Header("StagesJSON string Questions ")]
    [SerializeField]
    string[] stage1;        //stage1[0] = Stage 1-level 1 questions, stage1[1] = level 2 questions, stage1[2] = level 3 questions, stage1[3] = level 4 questions.
    [SerializeField]
    string[] stage2;
    [SerializeField]
    string[] stage3;
    [SerializeField]
    string[] stage4;

    Dictionary<int, string[]> stagesJsonQuestions=new();


    void Start()
    {
        stagesJsonQuestions = new Dictionary<int, string[]>
        {
            { 1, stage1 },
            { 2, stage2 },
            { 3, stage3 },
            { 4, stage4 },
        };      
    }

    public Question[] GenerateQuestions(int stage, int[] levels) // Returns list of question.   If int stage = 1 and int[] levels= { 1, 2 },
                                                                //                              function GenerateQuestions(stage, levels) will returns combined questions of level 1 and 2 in Stage 1
    {
        Question[] questions = { };

        foreach (var level in levels)
        {
            Question[] addQuestion = JsonConvert.DeserializeObject<Question[]>(stagesJsonQuestions[stage][level]);
            questions = questions.Concat(addQuestion).ToArray();
        }
        return questions;
    }
}

public class Question
{
    public string question;
    public List<string> choices;
    public string answer;
}