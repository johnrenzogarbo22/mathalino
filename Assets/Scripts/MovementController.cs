using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public float walkSpeed = 5f;
    public float jumpForce = 5f;
    int walk = 0;
    public Foot foot;
    Rigidbody2D rb;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>(); 
    }

    void Update()
    {
        if (walk != 0)
        {
            transform.Translate(walk * Vector3.right * walkSpeed * Time.deltaTime);
            Vector3 newScale = transform.localScale;
            newScale.x = walk;
            transform.localScale = newScale;
        }
    }

    public void Walk(int x)
    {
        walk = x;
    }

    public void Jump()
    {
        if (foot.grounded)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);     
        }
    }
}
