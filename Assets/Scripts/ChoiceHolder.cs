using UnityEngine;
using TMPro;
public class ChoiceHolder : MonoBehaviour
{
    public string value="";
    public TextMeshProUGUI text;
    public void SetText(string x)
    {
        value = x;
        text.text = x;
    }
}
