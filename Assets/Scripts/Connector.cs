using UnityEngine;

public class Connector : MonoBehaviour
{
    public GameObject controller;
    public QuestionsData questionData;
    public UIController uiController;
    public PlayerController playerController;
    public GameObject player;
    void Start()
    {
        Invoke("Connect", 0.1f);
    }

    void Connect()
    {
        controller = GameObject.Find("PlayerController");
        uiController = controller.GetComponent<UIController>();
        playerController = controller.GetComponent<PlayerController>();
        questionData = playerController.gameData.GetComponent<QuestionsData>();
        player = playerController.player;
    }
}
