using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ColliderEventTrigger : MonoBehaviour
{
    public string tag = "Player";
    public UnityEvent onCollisionEnter = new UnityEvent(); 
    public UnityEvent onCollisionExit = new UnityEvent(); 

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag==tag)
        {
            Debug.Log(tag);
            onCollisionEnter.Invoke();
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == tag)
        {           
            onCollisionExit.Invoke();
        }
    }
}
